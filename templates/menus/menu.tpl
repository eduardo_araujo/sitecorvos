
<header id="header">
    <nav class="navbar ">
                            </nav>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header ">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="?acao=home">
                    <img src="images/logo/Logo_Corvos.png">
                </a>

            </div>
            <div id="navbar" class="navbar-collapse collapse">


                <div class="social">

                    <a href="https://www.facebook.com/Corvos-432041397003515/?fref=ts" target="_blank" class="btn btn-social-icon btn-facebook">
                        <span class="fa fa-facebook"></span>
                    </a>

                    <a href="https://twitter.com/teccorvos" target="_blank" class="btn btn-social-icon btn-twitter">
                        <span class="fa fa-twitter"></span>
                    </a>

                    <a href="https://www.instagram.com/teccorvos/" target="_blank" class="btn btn-social-icon btn-instagram">
                        <span class="fa fa-instagram"></span>
                    </a>

                    <a href="https://corvosteam.slack.com/" target="_blank" class="btn btn-social-icon btn-slack">
                        <span class="fa fa-slack"></span>
                    </a>

                </div>


                <ul class="nav navbar-nav">

                    <li{if $pagina == 'home'} class="active"{/if}><a href="?acao=home">Home</a></li>
                    <li{if $pagina == 'quemsomos'} class="active"{/if}><a href="?acao=quemsomos">Quem Somos</a></li>

                    <li class="dropdown">
                        <a href="?acao=servicos" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Serviços <span class="caret"></span></a>
                        <ul class="dropdown-menu">

                            <li><a href="?acao=servicos">TODOS</a></li>
                            <li role="option" class="divider"></li>
                            <li><a href="?acao=interna_mobile">Aplicativos mobile</a></li>
                            <li><a href="?acao=interna_sites">Sites responsivos</a></li>
                            <li><a href="?acao=interna_visual">Identidade visual</a></li>
                            <li><a href="?acao=interna_ecommerce">E-Commerce</a></li>
                            <li><a href="?acao=interna_mvp">MVP</a></li>

                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="?acao=solucoes" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Soluções <span class="caret"></span></a>
                        <ul class="dropdown-menu">

                            <li><a href="?acao=solucoes">TODOS</a></li>
                            <li role="option" class="divider"></li>
                            <li><a href="?acao=interna_marketplace">Marketplace </a></li>
                            <li><a href="?acao=interna_chamados">Chamados</a></li>
                            <li><a href="?acao=interna_projetos">Gerenciador de projetos</a></li>
                            <li><a href="?acao=interna_mei">MicroEmpreendedor (MEI)</a></li>


                        </ul>
                    </li>
                    <li{if $pagina == 'clientes'} class="active"{/if}><a href="?acao=clientes">Clientes</a></li>
                    <li{if $pagina == 'templates'} class="active"{/if}><a href="?acao=templates">Modelos</a></li>

                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contato <span class="caret"></span></a>

                        <ul class="dropdown-menu">


                            <li><a href="?acao=contato">Contato Direto</a></li>
                            <li><a href="#">contato@corvos.xyz</a></li>
                            <li role="option" class="divider"></li>
                            <li><a href="#">(11)&nbsp;96067.6769 - Eduardo</a></li>
                            <li><a href="#">(11)&nbsp;94287.0173 - Xavier</a></li>
                            <li><a href="#">(11)&nbsp;98272.8570 - Danilo</a></li>

                        </ul>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
</header>