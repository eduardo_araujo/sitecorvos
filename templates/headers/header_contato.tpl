<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Corvos</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Language" content="pt-br">
    <link rel="icon" href="images/faavicon.ico" >
    <link rel="stylesheet" type="text/css" href="biblioteca/jquery_ui/css/base/jquery-ui-1.9.2.custom.css">
    <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap-social-gh-pages/bootstrap-social.css">
    <link rel="stylesheet" type="text/css" href="biblioteca/font-awesome-4.5.0/css/font-awesome.css">
    <link rel="stylesheet" href="css/touchTouch.css">
    <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsivo.css">


    <script src="js/jquery.js"></script>
    <script src="js/touchTouch.jquery.js"></script>
    <script src="js/script.js"></script>
    <script src="biblioteca/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/index.js"></script>
    <script src="js/contato.js"></script>

</head>

<body class="index">

        {include file="menus/menu.tpl"}
