<!DOCTYPE html>
<html lang="">
<head>
    <title>Corvos Tecnologia</title>
    <meta charset="utf-8">
    <meta content="A Corvos Tecnologia veio ao mercado para mostrar que com valor justo e trabalhando junto com o cliente podemos entregar um serviço de qualidade atendendo a todas as necessidades." name="description">
    <meta http-equiv="Content-Language" content="pt-BR">
    <link rel="icon" href="images/faavicon.ico" >
    <link rel="alternate" href="http://corvostecnologia.com.br" hreflang="pt-BR" />
    <link rel="stylesheet" type="text/css" href="biblioteca/jquery_ui/css/base/jquery-ui-1.9.2.custom.css">
    <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap-social-gh-pages/bootstrap-social.css">
    <link rel="stylesheet" type="text/css" href="biblioteca/font-awesome-4.5.0/css/font-awesome.css">
    <link rel="stylesheet" href="css/touchTouch.css">
    <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="biblioteca/ionicons-2.0.1/ionicons-2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsivo.css">

    <script src="js/jquery.js"></script>
    <script src="biblioteca/jquery_ui/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/touchTouch.jquery.js"></script>
    <script src="biblioteca/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/index.js"></script>
    <!-- Google Tag Manager -->
    {literal}
        <noscript>
            <iframe src="//www.googletagmanager.com/ns.html?id=GTM-PLMZLB"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-PLMZLB');</script>
        <!-- End Google Tag Manager -->
    {/literal}
</head>
<body class="index">

        {include file="menus/menu.tpl"}
