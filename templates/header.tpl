<html>
    <head>
        <title>Corvos</title>
        <meta charset="utf-8">
        <meta name = "format-detection" content = "telephone=no" />

        {if $pagina == 'home'}
            <link rel="icon" href="images/faavicon.ico" >
            <link rel="stylesheet" type="text/css" href="biblioteca/jquery_ui/css/base/jquery-ui-1.9.2.custom.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap-social-gh-pages/bootstrap-social.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/font-awesome-4.5.0/css/font-awesome.css">
            <link rel="stylesheet" href="css/touchTouch.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap-theme.css">
            <link rel="stylesheet" href="css/animate.css">
            <link rel="stylesheet" href="css/style.css">
            <link rel="stylesheet" href="css/home.css">
            <link rel="stylesheet" href="css/responsivo.css">

            <script src="js/jquery.js"></script>
            <script src="js/touchTouch.jquery.js"></script>
            <script src="js/script.js"></script>
            <script src="biblioteca/bootstrap/js/bootstrap.min.js"></script>
            <script src="js/index.js"></script>
        {/if}

        {if $pagina == 'quemsomos'}
            <link rel="stylesheet" href="css/quemsomos.css">
        {/if}

        {if $pagina == 'servicos'}
            <link rel="icon" href="images/faavicon.ico" >
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap-theme.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap-social-gh-pages/bootstrap-social.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/font-awesome-4.5.0/css/font-awesome.css">
            <link rel="stylesheet" href="biblioteca/onePageApresentaton/assets/css/main.css">
            <link rel="stylesheet" href="css/style.css">
            <link rel="stylesheet" href="css/touchTouch.css">
            <link rel="stylesheet" href="css/servicos.css">
            <link rel="stylesheet" href="css/responsivo.css">

            <script src="js/jquery.js"></script>
            <script src="js/script.js"></script>
            <script src="biblioteca/bootstrap/js/bootstrap.min.js"></script>
        {/if}

        {if $pagina == 'clientes'}
            <link rel="stylesheet" type="text/css" href="biblioteca/jquery_ui/css/base/jquery-ui-1.9.2.custom.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap-social-gh-pages/bootstrap-social.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/font-awesome-4.5.0/css/font-awesome.css">
            <link rel="stylesheet" href="css/touchTouch.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap-theme.css">
            <link rel="stylesheet" href="css/animate.css">
            <link rel="stylesheet" href="css/style.css">
            <link rel="stylesheet" href="css/clientes.css">
            <link rel="stylesheet" href="css/responsivo.css">

            <script src="js/jquery.js"></script>
            <script src="js/jquery.gallery.js"></script>
            <script src="js/modernizr.custom.53451.js"></script>
            <script src="js/touchTouch.jquery.js"></script>
            <script src="js/script.js"></script>
            <script src="biblioteca/bootstrap/js/bootstrap.min.js"></script>
            <script src="js/index.js"></script>
        {/if}

        {if $pagina == 'templates'}
            <link rel="stylesheet" type="text/css" href="biblioteca/jquery_ui/css/base/jquery-ui-1.9.2.custom.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap-social-gh-pages/bootstrap-social.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/font-awesome-4.5.0/css/font-awesome.css">
            <link rel="stylesheet" href="css/touchTouch.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap-theme.css">
            <link rel="stylesheet" href="css/animate.css">
            <link rel="stylesheet" href="css/style.css">
            <link rel="stylesheet" href="css/templates.css">
            <link rel="stylesheet" href="css/responsivo.css">

            <script src="js/jquery.js"></script>
            <script src="js/touchTouch.jquery.js"></script>
            <script src="js/script.js"></script>
            <script src="biblioteca/bootstrap/js/bootstrap.min.js"></script>
            <script src="js/index.js"></script>
        {/if}

        {if $pagina == 'contato'}
            <link rel="stylesheet" type="text/css" href="biblioteca/jquery_ui/css/base/jquery-ui-1.9.2.custom.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap-social-gh-pages/bootstrap-social.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/font-awesome-4.5.0/css/font-awesome.css">
            <link rel="stylesheet" href="css/touchTouch.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap-theme.css">
            <link rel="stylesheet" href="css/animate.css">
            <link rel="stylesheet" href="css/style.css">
            <link rel="stylesheet" href="css/home.css">
            <link rel="stylesheet" href="css/responsivo.css">
            <link rel="stylesheet" href="css/contact-form.css">
            <link rel="stylesheet" href="css/responsivo.css">

            <script src="js/jquery.js"></script>
            <script src="js/touchTouch.jquery.js"></script>
            <script src="js/script.js"></script>
            <script src="biblioteca/bootstrap/js/bootstrap.min.js"></script>
            <script src="js/index.js"></script>
            <script src="js/contato.js"></script>
        {/if}

        {if $pagina == 'quemsomos'}
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap-social-gh-pages/bootstrap-social.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/font-awesome-4.5.0/css/font-awesome.css">
            <link rel="stylesheet" href="css/touchTouch.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/bootstrap/css/bootstrap-theme.css">
            <link rel="stylesheet" type="text/css" href="biblioteca/jquery_ui/css/base/jquery-ui-1.9.2.custom.css">
            <link rel="stylesheet" href="css/animate.css">
            <link rel="stylesheet" href="css/style.css">
            <link rel="stylesheet" href="css/quemsomos.css">
            <link rel="stylesheet" href="css/responsivo.css">

            <script src="js/jquery.js"></script>
            <script src="biblioteca/jquery_ui/js/jquery-ui-1.9.2.custom.js"></script>
            <script src="js/script.js"></script>
            <script src="biblioteca/bootstrap/js/bootstrap.min.js"></script>
            <script src="js/index.js"></script>
            <script src="js/quemsomos.js"></script>
        {/if}


    </head>

    <body class="index">

        {include file="menus/menu.tpl"}
