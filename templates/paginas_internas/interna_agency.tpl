{config_load file="test.conf" section="setup"}

{include file="headers/header_home.tpl" title=foo}

<body>

<div id="content">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-uppercase">Modelo<label class="services">&nbsp;Agency</label></h1>
            <p class="sub"><span>Totalmente Responsivo</span><div class="linha"></div></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="templates">

                <div class="col-lg-6">
                    <img data-src="holder.js/200x200" class="img-thumbnail" alt="200x200" src="images/Templates/Agency.JPG" data-holder-rendered="true" style="width: 550px; height: 290px;">


                    <ul class="botoes">

                    <li><a href="templates_site/agency/index.html" target="_blank"><p><button type="button" class="btn btn-large btn-info navegar">Navegar</button></p></a></li>
                    <li class="comprar"><a href="contato.html" target="_blank"><p><button type="button" class="btn btn-large btn-info navegar">Comprar</button></p></a></li>


                    </ul>
                </div>
                <div class="col-lg-6">
                    <ul class="info_template">
                        <h1 class="subtitulo">Corvos Agency</h1>
                        <li class="valor"><strong class="valor1">Valor:</strong>&nbsp;R$ 899,00</li>
                        <li class="valor2"><strong>Info:&nbsp;</strong>Template moderno e versátil. </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>

{include file="footer.tpl"}

{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77183084-3', 'auto');
        ga('send', 'pageview');

    </script>
{/literal}

</body>
</html>