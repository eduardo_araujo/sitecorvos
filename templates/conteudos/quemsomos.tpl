<!--=======content================================-->

<section id="content">
    <div class="col-md-12 interna">
        <header >
            <h1 class="we titulo_interna">Nosso<span>Time</span></h1>
        </header>
        <div class="col-md-12 pad">
            <div class="col-md-12 border-top">&nbsp;</div>
        </div>
    </div>

    <div class="full-width-container block-1">
        <div class="container ">


                <div class="col-md-12 time">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 text-center">
                                <div class="service-box">
                                    <img src="images/danxavier.jpg" alt="Danilo Xavier" class="profile">
                                    <h3>Danilo Xavier</h3>
                                    <p class="text-muted">Designer com especialidade em aplicações web, mobile, design UX e criação de identidade visual . A área criativa de sua empresa estará em boas mãos.
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 text-center">
                                <div class="service-box">
                                    <img src="images/du.JPG" alt="Eduardo Henrique" class="profile" />
                                    <h3>Eduardo Henrique</h3>
                                    <p class="text-muted">Atuando com sistemas web e websites desde os 17 anos, já viveu diversas tecnologias como Javascript, PHP, Java, HTML5, CSS3, entre outros. Possui experiência tanto em empresas multinacionais (TOTVS) quanto em empresas menores conhecendo as peculiaridades de cada ambiente.</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 text-center">
                                <div class="service-box">
                                    <img src="images/dansavio.jpg" alt="Danilo Savio" class="profile" />
                                    <h3>Danilo Savio</h3>
                                    <p class="text-muted">
                                        Possui vasta experiência na área de TI, atuando em diversos setores, trabalhando em empresas pequenas e de porte multinacional (Capgemini), conhecendo os diversos cenários do dia a dia destas corporações. Encontrou a sua paixão pelos aplicativos móveis em 2012. As possibilidades do universo Mobile são infinitas!
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-md-12 valores_empresa">
                    <header>
                        <h1 class="we">Arte,<span> Inovação</span> e Tecnologia</h1>
                    </header>
                    <div class="container valores_empresa">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 text-center">
                                <div class="service-box">
                                    <i class="fa fa-4x fa-diamond wow bounceIn text-primary"></i>
                                    <h3>Qualidade</h3>
                                    <p class="text-muted">Oferecer produtos com qualidade promovendo uma nova perspectiva de como uma organização pode se conectar a tecnologia.</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 text-center">
                                <div class="service-box">
                                    <i class="fa fa-4x fa-paper-plane wow bounceIn text-primary" data-wow-delay=".1s"></i>
                                    <h3>Vamos crescer com você</h3>
                                    <p class="text-muted">Ser a referência em soluções de tecnologia no mercado proporcionando a todas organizações, independentemente do seu porte, recursos para alavancar seus negócios.</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 text-center">
                                <div class="service-box">
                                    <i class="fa fa-4x fa-newspaper-o wow bounceIn text-primary" data-wow-delay=".2s"></i>
                                    <h3>Sucesso do Cliente</h3>
                                    <p class="text-muted">
                                        <ul style="text-align: left">
                                            <li>Satisfação em saber que nosso clientes evoluiram devido aos nossos produtos.</li>
                                            <li>Transparência nas negociações e na comunicação.</li>
                                        </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77183084-3', 'auto');
        ga('send', 'pageview');

    </script>
{/literal}
