<section id="content">

    <div class="col-md-12 interna">
        <header >
            <h1 class="we titulo_interna">Nossos <span>Serviços</span></h1>
        </header>
        <div class="col-md-12 pad">
            <div class="col-md-12 border-top">&nbsp;</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="col-md-6">
                <div class="panel painel">
                    <div class="panel-heading titulo">Aplicativos Mobile</div>
                    <div class="panel-body">

                        <div class="subtittle subtitulo">Você conhece o Ionic ?</div>

                        <ul class="media-list">
                            <li class="media">

                                <div class="col-md-4">

                                    <img class="media-object img_solucao" src="images/serviços/aplicativos_mobile.jpg" alt="aplicativos mobile">
                                    </a>
                                </div>

                                <div class="col-md-8">
                                    <h4 class="media-heading">O <a href=”http://ionicframework.com/” target="_blank">Ionic Framework</a> está quebrando os paradigmas do desenvolvimento mobile. Através da linguagem Web é possível construir Apps Híbridos de forma produtiva e facilitada.</h4>
                                </div>

                                <div class="col-md-12">

                                    <!-- eu tirei a classe btn-default mas mantive a btn e a botao  -->
                                   <a href="?acao=interna_mobile"><button type="button" class="btn  botao">Saiba Mais</button></a>


                                </div>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="panel painel">
                    <div class="panel-heading titulo">Sites Responsivos</div>
                    <div class="panel-body">

                        <div class="subtittle subtitulo">O que são sites responsivos?</div>

                        <ul class="media-list">
                            <li class="media">
                               <div class="col-md-4">
                                        <img class="media-object img_solucao" src="images/serviços/site_responsivo.jpg" alt="sites responsivos">
                                </div>

                                <div class="col-md-8">
                                    <h4 class="media-heading">Vivemos em mundo em que os Smartphones e Tablets estão cada vez mais presentes e um site responsivo se adapta a estes dispositivos, aumentando sua flexibilidade e ganhando mais e mais visitas.</h4>
                                </div>

                                <div class="col-md-12">

                                    <!-- eu tirei a classe btn-default mas mantive a btn e a botao  -->
                                    <a href="?acao=interna_sites"><button type="button" class="btn  botao">Saiba Mais</button></a>

                                </div>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>





        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="col-md-6">
                <div class="panel painel">
                    <div class="panel-heading titulo">Identidade Visual</div>
                    <div class="panel-body">

                        <div class="subtittle subtitulo">O que é identidade visual?</div>

                        <ul class="media-list">
                            <li class="media">

                                <div class="col-md-4">
                                    <a href="#">
                                        <img class="media-object img_solucao" src="images/serviços/identidade_visual.jpg" alt="identidade visual">
                                    </a>
                                </div>

                                <div class="col-md-8">
                                    <h4 class="media-heading">O sucesso de uma empresa está ligado diretamente a forma como suas idéias e conceitos são transmitidos ao público e seus clientes. A identidade visual representa a personalidade da sua empresa através de recursos visuais e gráficos.</h4>
                                </div>

                                <div class="col-md-12">

                                    <!-- eu tirei a classe btn-default mas mantive a btn e a botao  -->
                                    <a href="?acao=interna_visual"><button type="button" class="btn  botao">Saiba Mais</button></a>


                                </div>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel painel">
                    <div class="panel-heading titulo">E-Commerce</div>
                    <div class="panel-body">

                        <div class="subtittle subtitulo">Qual a vantagem de uma loja virtual?</div>

                        <ul class="media-list">
                            <li class="media">

                                <div class="col-md-4">
                                    <a href="#">
                                        <img class="media-object img_solucao" src="images/serviços/ecommerce.jpg" alt="sites responsivos">
                                    </a>
                                </div>

                                <div class="col-md-8">
                                    <h4 class="media-heading">Com uma loja virtual é possível vender seus produtos e serviços na internet , 24 horas por dia,  para um público  que pode ser global ou limitado de acordo com sua escolha.</h4>
                                </div>

                                <div class="col-md-12">

                                    <!-- eu tirei a classe btn-default mas mantive a btn e a botao  -->
                                    <a href="?acao=interna_ecommerce"><button type="button" class="btn  botao">Saiba Mais</button></a>


                                </div>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="col-md-6">
                <div class="panel painel">
                    <div class="panel-heading titulo">MVP</div>
                    <div class="panel-body">

                        <div class="subtittle subtitulo">O que é MVP ?</div>

                        <ul class="media-list">
                            <li class="media">

                                <div class="col-md-4">
                                    <a href="#">
                                        <img class="media-object img_solucao" src="images/serviços/img_mvp.jpg" alt="MVP">
                                    </a>
                                </div>

                                <div class="col-md-8">
                                    <h4 class="media-heading">O MVP, do inglês Minimum Viable Product (Produto mínimo viável), é uma versão do seu produto com um conjunto mínimo de características necessárias para que ele
                                        possa ser colocado de imediato no ar e submetido a testes que permitirão validá-lo e aprimorá-lo.</h4>
                                </div>

                                <div class="col-md-12">

                                    <!-- eu tirei a classe btn-default mas mantive a btn e a botao  -->
                                    <a href="?acao=interna_mvp"><button type="button" class="btn  botao">Saiba Mais</button>


                                </div>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>


        </div>
    </div>



</section>

{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77183084-3', 'auto');
        ga('send', 'pageview');

    </script>
{/literal}
