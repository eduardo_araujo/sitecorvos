<div id="content">
<div class="col-md-12 interna">
    <header>
        <h2>Últimos Trabalhos</h2>
    </header>
    <div class="col-md-12 pad">
        <div class="col-md-12 border-top">&nbsp;</div>
    </div>
    <div class="col-md-12"  style="background: rgb(45, 42, 43);">

            <div class="row" >

                        <div id="carrousel_services" class="carousel slide c_servicos">

                            <ol class="carousel-indicators">
                                <li data-target="#carrousel_services" data-slide-to="0" class="active"></li>
                                <li data-target="#carrousel_services" data-slide-to="1"></li>

                            </ol>

                            <!-- Carousel items -->
                            <div class="carousel-inner" >

                                <div class="item active">
                                    <div class="row-fluid">
                                        <div class="span3"><a href="#x" class="thumbnail"><div class="img_block"><img src="images/clientes/Interdex.png" alt="Interdex"></div></a></div>
                                        {*<div class="span3"><a href="#x" class="thumbnail"><div class="img_block"><img src="images/ERP%20Cloud%20-%20Sistema%20integrado%20de%20gestão.png" alt="img"></div></a></div>*}
                                        <div class="span3"><a href="#x" class="thumbnail"><div class="img_block"><img src="images/Globes%20-%20Home.png" alt="Globes"></div></a></div>
                                        <div class="span3"><a href="#x" class="thumbnail"><div class="img_block"><img src="images/Canaa%20Brazil.png" alt="Canaa Brazil"></div></a></div>
                                    </div><!--/row-fluid-->
                                </div><!--/item-->

                                <div class="item">
                                    <div class="row-fluid">
                                        <div class="span3"><a href="#x" class="thumbnail""><div class="img_block"><img src="images/DYNAMIC%20SALE%20-%20LINKS%20PATROCINADOS.png" alt="Dinamic Sale"></div></a></div>
                                        <div class="span3"><a href="#x" class="thumbnail""><div class="img_block"><img src="images/Modelo%201.jpg" alt="Modelo Corvos"></div></a></div>
                                        <div class="span3"><a href="#x" class="thumbnail""><div class="img_block"><img src="images/benatisportes.jpg" alt="Benati Sportes"></div></a></div>

                                    </div><!--/row-fluid-->
                                </div><!--/item-->


                            </div><!--/carousel-inner-->

                            <a class="left carousel-control" href="#carrousel_services" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carrousel_services" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div><!--/myCarousel-->


            </div>

    </div>
</div>
{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77183084-3', 'auto');
        ga('send', 'pageview');

    </script>
{/literal}