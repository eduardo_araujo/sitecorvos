<div class="col-md-12">
    <header >
        <h1 class="we titulo_interna">Nossas<span> Soluções</span></h1>
    </header>
    <div class="col-md-12 pad">
        <div class="col-md-12 border-top">&nbsp;</div>
    </div>
</div>


<div class="container_servicos">
    <div id="quemsomos" class="full-width-container block-3">
        <div class="container">
            <div class="row">
                <div class="container servicos_home" >
                      <div class="row">
                          <a href="?acao=interna_sites">
                              <div class="col-lg-4 col-md-4 text-center">
                                <div class="service-box sites">
                                    <i class="fa fa-4x fa-laptop"></i>
                                    <h3>Sites Responsivos</h3>
                                    <p >Seu site adaptado ao mundo mobile.</p>
                                </div>
                            </div>
                          </a>
                          <a href="?acao=interna_mobile">
                              <div class="col-lg-4 col-md-4 text-center">
                                    <div class="service-box ecommerce">
                                        <i class="fa fa-4x fa-mobile"></i>
                                        <h3>Aplicativos Mobile</h3>
                                        <p>Sua solução na palma da mão.</p>
                                    </div>
                              </div>
                           </a>
                          <a href="?acao=interna_visual">
                            <div class="col-lg-4 col-md-4 text-center">
                                <div class="service-box mobile">
                                    <i class="fa fa-4x fa-paint-brush"></i>
                                    <h3>Identidade Visual</h3>
                                    <p >Não basta ser visto, tem que ser lembrado.</p>
                                </div>
                            </div>
                          </a>

                          <a href="?acao=interna_ecommerce">
                            <div class="col-lg-4 col-md-4 text-center">
                                <div class="service-box idevisual">
                                    <i class="fa fa-4x fa-cart-plus"></i>
                                    <h3>E-Commerce</h3>
                                    <p >Venda na Web, 24 horas por dia.</p>
                                </div>
                            </div>
                          </a>

                          <a href="?acao=interna_chamados">
                            <div class="col-lg-4 col-md-4 text-center">
                                <div class="service-box midia">
                                    <i class="fa fa-4x fa-users"></i>
                                    <h3>Chamados</h3>
                                    <p >Abrir um chamado nunca foi tão simples.</p>
                                </div>
                            </div>
                          </a>
                          <a href="?acao=interna_projetos">
                            <div class="col-lg-4 col-md-4 text-center">
                                <div class="service-box seo">
                                    <i class="fa fa-4x fa-line-chart"></i>
                                    <h3>Gerenciamento de Projetos</h3>
                                    <p >Gerencie seus projetos e potencialize seus recursos.</p>
                                </div>
                            </div>
                          </a>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77183084-3', 'auto');
        ga('send', 'pageview');

    </script>
{/literal}
