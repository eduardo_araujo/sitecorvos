    <section id="content">

        <div class="col-md-12 interna">
            <header >
                <h1 class="we titulo_interna">Nossas <span>Soluções</span></h1>
            </header>
            <div class="col-md-12 pad">
                <div class="col-md-12 border-top">&nbsp;</div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="col-md-6">
                    <div class="panel painel">
                        <div class="panel-heading titulo">Marketplace</div>
                            <div class="panel-body">

                                <div class="subtittle subtitulo">O que é marketplace ?</div>

                                    <ul class="media-list">
                                        <li class="media">

                                            <div class="col-md-4">

                                                    <img class="media-object img_solucao" src="images/marketplace.jpg" alt="marketplace">
                                                </a>
                                            </div>

                                            <div class="col-md-8">
                                                <h4 class="media-heading">O Marketplace é um aplicativo/sistema que interliga pessoas que precisam de um produto, serviço ou solução com quem pode oferecer o que elas buscam.</h4>
                                            </div>

                                           <div class="col-md-12">

<!-- eu tirei a classe btn-default mas mantive a btn e a botao  -->
                                                  <a href="?acao=interna_marketplace"><button type="button" class="btn  botao">Saiba Mais</button></a>


                                           </div>

                                        </li>
                                    </ul>
                            </div>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="panel painel">
                        <div class="panel-heading titulo">Chamados</div>
                        <div class="panel-body">

                            <div class="subtittle subtitulo">Quanto tempo seu cliente leva para abrir um chamado?</div>

                            <ul class="media-list">
                                <li class="media">

                                    <div class="col-md-4">
                                        <a href="#">
                                            <img class="media-object img_solucao" src="images/chamados.jpg" alt="chamado chamados qr code">
                                        </a>
                                    </div>

                                    <div class="col-md-8">
                                        <h4 class="media-heading">Com a nossa solução de chamados com QRCODE um chamado é aberto em questão de segundos, tudo isso através de uma plataforma Mobile e Web.</h4>
                                    </div>

                                    <div class="col-md-12">

                                        <!-- eu tirei a classe btn-default mas mantive a btn e a botao  -->
                                        <a href="?acao=interna_chamados"><button type="button" class="btn  botao">Saiba Mais</button></a>


                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>



            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="col-md-6">
                    <div class="panel painel">
                        <div class="panel-heading titulo">Gerenciamento de Projetos</div>
                        <div class="panel-body">

                            <div class="subtittle subtitulo">Como você gerencia seus projetos?</div>

                            <ul class="media-list">
                                <li class="media">

                                    <div class="col-md-4">
                                        <a href="#">
                                            <img class="media-object img_solucao" src="images/projetos.jpg" alt="projetos">
                                        </a>
                                    </div>

                                    <div class="col-md-8">
                                        <h4 class="media-heading">Gerenciar os projetos da sua empresa é essencial para otimizar seus prazos e custos,  conheça nossa ferramenta que possui as melhores práticas de gerenciamento do mercado.</h4>
                                    </div>

                                    <div class="col-md-12">

                                        <!-- eu tirei a classe btn-default mas mantive a btn e a botao  -->
                                        <a href="?acao=interna_projetos"> <button type="button" class="btn  botao">Saiba Mais</button></a>


                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel painel">
                        <div class="panel-heading titulo">Microempreendedor (MEI)</div>
                        <div class="panel-body">

                            <div class="subtittle subtitulo">Como nós podemos te ajudar?</div>

                            <ul class="media-list">
                                <li class="media">

                                    <div class="col-md-4">
                                        <a href="#">
                                            <img class="media-object img_solucao" src="images/serviços/MEI.jpg" alt="Microoempreendedor ">
                                        </a>
                                    </div>

                                    <div class="col-md-8">
                                        <h4 class="media-heading">Com o nosso modelo de templates pré-definidos podemos entregar a você MEI, um site responsivo de qualidade por um preço muito mais acessível.</h4>
                                    </div>

                                    <div class="col-md-12">

                                        <!-- eu tirei a classe btn-default mas mantive a btn e a botao  -->
                                        <a href="?acao=interna_mei"> <button type="button" class="btn  botao">Saiba Mais</button><a/>


                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>

    {literal}
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-77183084-3', 'auto');
            ga('send', 'pageview');

        </script>
    {/literal}
