
<div class="item active i_servicos i_chamados">
    <div class="content">
        <div class="col-md-12 ">
            <div class="col-md-6 text">
                Com nossa solução um chamado é aberto em questão de segundos.
            </div>
            {*<div class="col-md-6 link">*}
                {*<!-- Button trigger modal -->*}
                {*<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">*}
                    {*Veja o Video <i class="fa fa-play-circle"></i>*}
                {*</button>*}
            {*</div>*}
        </div>
    </div>
</div>

<section id="content ">
    <div class="col-md-12 interna">
        <header >
            <h1 class="we titulo_interna">Cham<span>ados</span></h1>
        </header>
        <div class="col-md-12 pad">
            <div class="col-md-12 border-top">&nbsp;</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <img class="img_app img_mobile " src="images/img_chamados.jpg">
        </div>
        <div class="col-md-6 interna_direita">

            <div id="accordion">

                <h3 class="sanfona">Como funciona?</h3>
                <div>

                    Nossa solução de chamados consiste em uma plataforma Web e Mobile, um chamado é aberto utilizando a tecnologia QRCODE, que pode ser adaptada a qualquer tipo de negócio.
                    Uma solução de ponta-a-ponta que conta com interface para o usuário que abrirá o chamado e também para a empresa que receberá e responderá os chamados.

                </div>
                <h3 >Quanto tempo seu cliente leva para abrir um chamado?</h3>
                <div>
                    <p>
                        Com a nossa solução de chamados com QRCODE um chamado é aberto em questão de segundos, tudo isso através de uma plataforma Mobile e Web.
                    </p>
                </div>
                <h3>Tecnologias Utilizadas</h3>
                <div>
                    <p>
                        <ul class="icons_serviços">
                            <li><i class="ion-ionic i_sanfona i_ionic"></i> Ionic</li>
                            <li><i class="i_sanfona"><div class="i_cordova "></div></i>Cordova</li>
                            <li><i class="ion-social-html5 i_sanfona i_html"></i> HTML5 </li>
                            <li><i class="ion-social-css3 i_sanfona i_css"></i> CSS3 </li>
                            <li><i class="ion-social-javascript i_sanfona i_js"></i> Javascript </li>
                            <li><i class="ion-social-angular i_sanfona i_angular"></i> AngularJS </li>
                            <li><i class="i_sanfona"><div class="i_php7 "></div></i>PHP7</li>
                            <li><i class="i_sanfona"><div class="i_laravel"></div></i>Laravel</li>
                        </ul>
                    </p>
                </div>
            </div>

        </div>

    </div>

    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="myModalLabel">Apresentação</h4>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/_muIDxxlFqQ" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary botao_orçamento">Orçamento</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>

            </div>
        </div>
    </div>
</div>

{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77183084-3', 'auto');
        ga('send', 'pageview');

    </script>
{/literal}


<script>
    $(function() {
        $( "#accordion" ).accordion({ autoHeight: false });
    });
</script>




