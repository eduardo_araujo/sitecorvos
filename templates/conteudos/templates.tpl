
<div id="content">
    <div class="col-md-12">
        <header >
            <h1 class="we titulo_interna">Nossos <span>Templates</span></h1>
        </header>
        <div class="col-md-12 pad">
            <div class="col-md-12 border-top">&nbsp;</div>
        </div>
    </div>


    <div class="row">

        <div class="col-md-12 templates">


            <div class="col-lg-4 col-md-4 text-center">
                <a href="?acao=template_knight">
                    <img data-src="holder.js/200x200" class="img-thumbnail" alt="knight Corvos" src="images/Templates/knight.jpg" data-holder-rendered="true" style="width: 300px; height: 190px;"></a>
                <h3>CORVOS KNIGHT - R$ 899,00</h3>
            </div>

            <div class="col-lg-4 col-md-4 text-center">
                <a href="?acao=template_corlate">
                    <img data-src="holder.js/200x200" class="img-thumbnail" alt="corlate Corvos" src="images/Templates/corlate.jpg" data-holder-rendered="true" style="width: 300px; height: 190px;"></a>
                <h3>CORVOS CORLATE - R$ 1.099,00</h3>
            </div>



            <div class="col-lg-4 col-md-4 text-center">
                <a href="?acao=template_creative">
                    <img data-src="holder.js/200x200" class="img-thumbnail" alt="Creative Corvos" src="images/Templates/creative.JPG" data-holder-rendered="true" style="width: 300px; height: 190px;"></a>
                <h3>CORVOS CREATIVE - R$ 899,00</h3>
            </div>


            <div class="col-lg-4 col-md-4 text-center">
                <a href="?acao=template_business">
                    <img data-src="holder.js/200x200" class="img-thumbnail" alt="business Corvos" src="images/Templates/business.jpg" data-holder-rendered="true" style="width: 300px; height: 190px;"></a>
                <h3>CORVOS BUSINESS - R$ 899,00</h3>
            </div>

                <div class="col-lg-4 col-md-4 text-center ">
                    <a href="?acao=template_agency">
                        <img data-src="holder.js/200x200" class="img-thumbnail" alt="Agency Corvos" src="images/Templates/Agency.JPG" data-holder-rendered="true" style="width: 300px; height: 190px;">
                    </a>
                    <h3>CORVOS AGENCY - R$ 899,00</h3>
                </div>

                <div class="col-lg-4 col-md-4 text-center">
                    <a href="?acao=template_freela">
                    <img class="img-thumbnail" alt="Freela Corvos" src="images/Templates/freela.JPG" data-holder-rendered="true" style="width: 300px; height: 190px;">
                    </a>
                    <h3>CORVOS FREELANCER -  R$ 799,00</h3>
                </div>

                <div class="col-lg-4 col-md-4 text-center">
                    <a href="?acao=template_gray">
                    <img data-src="holder.js/200x200" class="img-thumbnail" alt="Gray Corvos" src="images/Templates/gray.jpg" data-holder-rendered="true" style="width: 300px; height: 190px;"></a>
                    <h3>CORVOS GRAY - R$ 799,00</h3>
                </div>

                <div class="col-lg-4 col-md-4 text-center">
                    <a href="?acao=template_casual">
                    <img data-src="holder.js/200x200" class="img-thumbnail" alt="Casual Corvos" src="images/Templates/casual.JPG" data-holder-rendered="true" style="width: 300px; height: 190px;"></a>
                    <h3>CORVOS CASUAL - R$ 799,00</h3>
                </div>

                <div class="col-lg-4 col-md-4 text-center">
                    <a href="?acao=template_stylish">
                    <img data-src="holder.js/200x200" class="img-thumbnail" alt="Stilish Corvos" src="images/Templates/stilish.JPG" data-holder-rendered="true" style="width: 300px; height: 190px;"></a>
                    <h3>CORVOS STYLISH - R$ 899,00</h3>
                </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info template_info" role="alert">
                <p> O nosso modelo de templates pré-definidos foi construído para melhor atendê-lo.</p>

                <p class="template"> Após a compra será realizado a elaboração dos seguintes itens do seu site de acordo com o template:</p>
                <ul class="regras">
                    <li><strong>1 - Identidade Visual:</strong> Alterações de cores, fontes, ícones, imagens e logotipo</li>
                    <li><strong>2 - Textos:</strong> Conteúdo escrito do site.</li>
                    <li><strong>3 - Criação do domínio:</strong> O endereço do seu site.</li>
                </ul>
            </div>
        </div>
    </div>
</div>
{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77183084-3', 'auto');
        ga('send', 'pageview');

    </script>
{/literal}
