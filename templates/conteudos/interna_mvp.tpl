
<div class="item active i_servicos i_mvp">
    <div class="content">
        <div class="col-md-12 ">
            <div class="col-md-6 text">
                Podemos ajudar sua startup construindo seu MVP!
            </div>
            {*<div class="col-md-6 link">*}
                {*<!-- Button trigger modal -->*}
                {*<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">*}
                    {*Veja o Video <i class="fa fa-play-circle"></i>*}
                {*</button>*}
            {*</div>*}
        </div>
    </div>
</div>

<section id="content">
    <div class="col-md-12 interna">
        <header >
            <h1 class="we titulo_interna">MV<span>P</span></h1>
        </header>
        <div class="col-md-12 pad">
            <div class="col-md-12 border-top">&nbsp;</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <img class="img_app " src="images/img_mvp.jpg">
        </div>
        <div class="col-md-6 interna_direita">

            <div id="accordion">

                <h3 class="sanfona">O que é MVP?</h3>
                <div>

                    O MVP, do inglês Minimum Viable Product (Produto mínimo viável), é uma versão do seu produto com um conjunto mínimo de características necessárias para que ele
                    possa ser colocado de imediato no ar e submetido a testes que permitirão validá-lo e aprimorá-lo.
                    Através dele podemos interagir diretamente com os possíveis clientes, aprendendo com quem irá pagar pelos serviços.
                    É possível construir desde um MVP de baixa, como uma landing page simples, até um MVP de alta, como um aplicativo móvel.
                    <br><br>
                    Leia mais em <a class="link" href="https://endeavor.org.br/mvp/" target="_blank">https://endeavor.org.br/mvp/</a>
                    <br><br>
                    Leita também em:<a class="link" href="http://revistapegn.globo.com/Revista/Common/0,,EMI331706-17180,00-O+QUE+E+UM+MVP+E+POR+QUE+ELE+E+UTIL+PARA+SUA+STARTUP.html"> http://revistapegn.globo.com/Revista/Common/0,,EMI331706-17180,00-O+QUE+E+UM+MVP+E+POR+QUE+ELE+E+UTIL+PARA+SUA+STARTUP.html </a>


                </div>

                <h3 >Como nós podemos te ajudar?</h3>
                <div>

                    <p>
                        Nós desenvolveremos o MVP para você por um preço justo, utilizando nosso conhecimento e experiência com aplicativos Web e Mobile.

                    </p>
                </div>

                <h3>Por que confiar em nós?</h3>
                <div>

                    <p>
                        Assim como o público que nos busca, também somos empreendedores e temos nossa própria Startup.
                        Sendo assim conhecemos a rotina e as dificuldades da vida incerta de um Startuper.
                    </p>

                </div>

                <h3>Tecnologias utilizadas</h3>
                <div>
                    <p>
                    <ul>
                        <li>Ionic</li>
                        <li>Cordova</li>
                        <li>HTML5</li>
                        <li>CSS3</li>
                        <li>Javascript</li>
                        <li>Bootstrap</li>
                        <li>Laravel</li>
                        <li>PHP7</li>
                        <li>PostgreSQL</li>
                    </ul>
                    </p>

                </div>
            </div>

        </div>

    </div>

    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="myModalLabel">Apresentação</h4>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/_muIDxxlFqQ" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary botao_orçamento">Orçamento</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>

            </div>
        </div>
    </div>
</div>
{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77183084-3', 'auto');
        ga('send', 'pageview');

    </script>
{/literal}




<script>
    $(function() {
        $( "#accordion" ).accordion({ autoHeight: false });
    });
</script>




