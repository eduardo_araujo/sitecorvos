
<div class="item active i_servicos i_mobile">
    <div class="content">
        <div class="col-md-12 ">
            <div class="col-md-6  text">
                Desenvolvemos aplicativos híbridos para dispositivos Android e iOS (iPhone) utilizando as tecnologias mais recentes.
            </div>
            {*<div class="col-md-6 link">*}
                {*<!-- Button trigger modal -->*}
                {*<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">*}
                    {*Veja o Video <i class="fa fa-play-circle"></i>*}
                {*</button>*}
            {*</div>*}
        </div>
    </div>
</div>

<section id="content">
    <div class="col-md-12 interna">
        <header >
            <h1 class="we titulo_interna">Aplicativos <span>Mobile</span></h1>
        </header>
        <div class="col-md-12 pad">
            <div class="col-md-12 border-top">&nbsp;</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <img class="img_app img_mobile" src="images/app_mobile.jpg">
        </div>
        <div class="col-md-6 interna_direita">

            <div id="accordion">

                <h3 class="sanfona">Tecnologia Híbrida</h3>
                <div>

                    Hoje é possível escolher duas abordagens no momento de desenvolver um aplicativo móvel: A arquitetura híbrida e a nativa.

                    Nós da Corvos utilizamos a arquitetura híbrida através do Ionic Framework para entregar aplicativos móveis de qualidade.

                </div>
                <h3 >O que é a arquitetura híbrida?</h3>
                <div>
                    <p>
                        A arquitetura híbrida funciona através de um framework/ferramenta que nos permite desenvolver aplicativos móveis multiplataforma, utilizando apenas uma única linguagem de programação.
                        Isto significa que através de uma única linguagem (web no caso do Ionic) é possível construir um app que rode em um dispositivo Android e iOS, por exemplo.
                    </p>
                </div>
                <h3>Vantagens</h3>
                <div>
                    <p>
                        Para melhor explicação vamos fazer um comparativo entre híbrido e nativo.
                    </p>
                    <p>
                        Consideremos  um cenário, onde é necessário desenvolver um aplicativo para Android e iOS (iPhone, iPad):
                    </p>
                    <p>
                        A arquitetura híbrida (Ionic)<br><br>

                        <ul>
                        <h1>Projeto 1</h1>
                        <li>1 - A equipe de desenvolvedores web faz a programação utilizando linguagem web (HTML, CSS e Javascript) em um único projeto.</li>
                        <li>2 - O Ionic framework faz com que esse código se torne nativo, para que eles possam ser executados no celular/tablet.</li>
                        <li> 3 - Temos versões para Android e iOS prontas.</li>
                    </ul>
                    </p>

                    <p>A arquitetura Nativa (Java e SWIFT/Objective C)</p>

                    <ul>
                        <h1>Projeto 1</h1>
                        <li>1 - A equipe de desenvolvedores Java faz a programação utilizando a linguagem Java para Android em um projeto.</li>
                        <li>2 - Geram através do Java o arquivo para ser executado no dispositivo.</li>
                        <li> 3 - Temos versão para Android.</li>
                    </ul>
                    </p>

                </div>
                <h3>Tecnologias Utilizadas</h3>
                <div>

                    <p>
                        <ul class="icons_serviços">
                            <li><i class="ion-ionic i_sanfona i_ionic"></i> Ionic</li>
                            <li><i class="i_sanfona"><div class="i_cordova "></div></i>Cordova</li>
                            <li><i class="ion-social-html5 i_sanfona i_html"></i> HTML5 </li>
                            <li><i class="ion-social-css3 i_sanfona i_css"></i> CSS3 </li>
                            <li><i class="ion-social-javascript i_sanfona i_js"></i> Javascript </li>
                        </ul>
                    </p>
                </div>
            </div>

        </div>

    </div>

    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="myModalLabel">Apresentação</h4>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/_muIDxxlFqQ" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary botao_orçamento">Orçamento</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>

            </div>
        </div>
    </div>
</div>

{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77183084-3', 'auto');
        ga('send', 'pageview');

    </script>
{/literal}



<script>
    $(function() {
        $( "#accordion" ).accordion({ autoHeight: false });
    });
</script>




