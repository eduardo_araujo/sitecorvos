<section id="content">

    <div class="col-md-12 interna">
        <header >
            <h1 class="we titulo_interna">Clie<span>ntes</span></h1>
        </header>

        <div class="col-md-12 pad">
            <div class="col-md-12 border-top">&nbsp;</div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="container container_cliente">
            <div class="row row_cliente">


                <div class="grid_12 grid_cliente">

                    <div class="containerclientes">

                        <div class="wrapper">

                            <div class="galleryItem">
                                <a href="http://vilamaori.com.br/" target="_blank">
                                    <div class="fakeImage maori"></div>
                                    <div class="galleryOverlay">
                                    <h2 class="galleryItemTitel">Vila Maori</h2>
                                    <p class="galleryItemIntro">Desenvolvemos projetos exclusivos com visual autêntico e inteligente.</p>
                                    </div>
                                </a>
                            </div>

                            <div class="galleryItem">
                                <a href="http://condominio.corvostecnologia.com.br/#/login" target="_blank">
                                    <div class="fakeImage aero"></div>
                                    <div class="galleryOverlay">
                                        <h2 class="galleryItemTitel">Condomínio Aeroporto</h2>
                                        <p class="galleryItemIntro">Plataforma de gerenciamento de condomínio</p>
                                    </div>
                                </a>
                            </div>

                            <div class="galleryItem">
                                <a href="#" target="_blank">
                                    <div class="fakeImage abc"></div>
                                    <div class="galleryOverlay">
                                        <h2 class="galleryItemTitel">ABC Lovers</h2>
                                        <p class="galleryItemIntro">Guia de entreterimento da grande ABC Paulista</p>
                                    </div>
                                </a>
                            </div>

                            <div class="galleryItem">
                                <a href="https://www.facebook.com/benatisports/?fref=ts" target="_blank">
                                    <div class="fakeImage benati"></div>
                                    <div class="galleryOverlay">
                                        <h2 class="galleryItemTitel">Benati Sports</h2>
                                        <p class="galleryItemIntro">Camisas de futebol a preços acessíveis.</p>
                                    </div>
                                </a>
                            </div>

                            <div class="galleryItem">
                                <a href="http://pontodoiphonepaulista.com.br/#/" target="_blank">
                                    <div class="fakeImage piphone"></div>
                                    <div class="galleryOverlay">
                                        <h2 class="galleryItemTitel">Ponto do iPhone</h2>
                                        <p class="galleryItemIntro">Assistência técnica de celulares, notebooks, macbooks, iPads e tablets.</p>
                                    </div>
                                </a>
                            </div>

                            <div class="galleryItem">
                                <a href="http://www.jbferros.com.br/#/" target="_blank">
                                    <div class="fakeImage jb"></div>
                                    <div class="galleryOverlay">
                                        <h2 class="galleryItemTitel">JB Ferros & Aços</h2>
                                        <p class="galleryItemIntro">Empresa referência no ramo de ferro e aço.</p>
                                    </div>
                                </a>
                            </div>

                            <div class="galleryItem">
                                <a href="https://play.google.com/store/apps/details?id=com.ionicframework.apdexter662763&hl=pt_BR" target="_blank">
                                    <div class="fakeImage interdex"></div>
                                    <div class="galleryOverlay">
                                        <h2 class="galleryItemTitel">Interdex</h2>
                                        <p class="galleryItemIntro">Um app onde você pode comprar ou vender peças para veículos.</p>
                                    </div>
                                </a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{literal}
    <script>
        $(".galleryItem").mouseenter(function() {
        var thisoverlay = $(this).find('.galleryOverlay');

        thisoverlay.stop(true, true).animate({
        height: '200',
        marginTop: '-220px'
        });
        });

        $(".galleryItem").mouseleave(function() {
        var thisoverlay = $(this).find('.galleryOverlay');

        thisoverlay.stop(true, true).animate({
        height: '30',
        marginTop: '-50px'
        });
        });
    </script>
{/literal}

{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77183084-3', 'auto');
        ga('send', 'pageview');

    </script>
{/literal}