
<div class="item active i_servicos i_mei">
    <div class="content">
        <div class="col-md-12 ">
            <div class="col-md-6 text">
                Temos a solução para você...
            </div>
            {*<div class="col-md-6 link">*}
                {*<!-- Button trigger modal -->*}
                {*<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">*}
                    {*Veja o Video <i class="fa fa-play-circle"></i>*}
                {*</button>*}
            {*</div>*}
        </div>
    </div>
</div>

<section id="content">
    <div class="col-md-12 interna">
        <header >
            <h1 class="we titulo_interna">Microempreendedor<span>(MEI)</span></h1>
        </header>
        <div class="col-md-12 pad">
            <div class="col-md-12 border-top">&nbsp;</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <img class="img_app " src="images/img_mei.jpg">
        </div>
        <div class="col-md-6 interna_direita">

            <div id="accordion">

                <h3 class="sanfona">Como nós podemos te ajudar?</h3>
                <div>

                    Com o nosso modelo de templates pré-definidos podemos entregar a você MEI, um site responsivo de qualidade por um preço muito mais acessível.


                </div>
                <h3 >Por que confiar em nós?</h3>
                <div>
                    <p>
                        Assim como o público que nos busca, também somos empreendedores. Temos consciência das dificuldades enfrentadas pelas empresas MEI , mas com a Corvos a tecnologia pode finalmente chegar até você.

                    </p>
                </div>
                <h3>Tecnologias utilizadas</h3>
                <div>
                    <p>
                    <ul class="icons_serviços">


                        <li><i class="ion-social-html5 i_sanfona i_html"></i> HTML5 </li>
                        <li><i class="ion-social-css3 i_sanfona i_css"></i> CSS3 </li>
                    </p>
                </div>
            </div>

        </div>

    </div>

    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="myModalLabel">Apresentação</h4>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/_muIDxxlFqQ" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary botao_orçamento">Orçamento</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>

            </div>
        </div>
    </div>
</div>


{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77183084-3', 'auto');
        ga('send', 'pageview');

    </script>
{/literal}


<script>
    $(function() {
        $( "#accordion" ).accordion();
    });
</script>




