<footer id="footer">
    <ul class="icons">

        <li><a href="https://twitter.com/TecCorvos" class="icon fa fa-twitter" target="_blank"><span class="label">Twitter</span></a></li>
        <li><a href="https://www.facebook.com/Corvos-432041397003515/" class="icon fa fa-facebook" target="_blank"><span class="label">Facebook</span></a></li>
        <li><a href="https://www.instagram.com/teccorvos/" class="icon fa fa-instagram"target="_blank"><span class="label">Instagram</span></a></li>
        <li><a href="https://corvosteam.slack.com/" class="icon fa fa-slack" target="_blank"><span class="label">Slack</span></a></li>
        <li><a href="http://corvos.xyz/contato.html" class="icon fa fa-envelope-o"><span class="label">Email</span></a></li>

    </ul>


        <ul class="copyright">
            <li>&copy; Corvos </li>
        </ul>
{*    {literal}
        <script>
            (function() {
                var cx = '017818712999297702142:jhcwacaa2ig';
                var gcse = document.createElement('script');
                gcse.type = 'text/javascript';
                gcse.async = true;
                gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(gcse, s);
            })();
        </script>
        <a><gcse:search></gcse:search></a>
    {/literal}*}
</footer>
</body>
<script>
    $(function(){
        $('#touch_gallery a').touchTouch();
    });
</script><!--/#wrapper-->
</html>