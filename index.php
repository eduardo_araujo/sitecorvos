<?php
error_reporting(1); // sem msg de erro
//error_reporting(E_ALL); // todas
require 'SmartyPHP/libs/Smarty.class.php';


$smarty = new Smarty;

//$smarty->force_compile = true;
$smarty->debugging = false;
$smarty->caching = true;
$smarty->cache_lifetime = 120;

$acao = $_GET['acao'];
if($acao){
    switch ($acao)
    {
        case 'home':

            $smarty->assign("pagina",'home');
            $smarty->display('home.tpl');
        break;

        case 'solucoes':

            $smarty->assign("pagina",'solucoes');
            $smarty->display('solucoes.tpl');
        break;


        case 'quemsomos':

            $smarty->assign("pagina",'quemsomos');
            $smarty->display('quemsomos.tpl');
        break;

        case 'servicos':
            echo $acao;
            $smarty->assign("pagina",'servicos');
            $smarty->display('servicos.tpl');
        break;

        case 'clientes':

            $smarty->assign("pagina",'clientes');
            $smarty->display('clientes.tpl');
        break;

        case 'templates':
            
            $smarty->assign("pagina",'templates');
            $smarty->display('templates.tpl');
        break;

        case 'contato':

            $smarty->assign("pagina",'contato');
            $smarty->display('contato.tpl');
        break;

        case 'interna_mobile':

            $smarty->assign("pagina",'servicos');
            $smarty->display('interna_mobile.tpl');
            break;

        case 'interna_sites':

            $smarty->assign("pagina",'servicos');
            $smarty->display('interna_sites.tpl');
            break;

        case 'interna_visual':

            $smarty->assign("pagina",'servicos');
            $smarty->display('interna_visual.tpl');
            break;

        case 'interna_ecommerce':

            $smarty->assign("pagina",'servicos');
            $smarty->display('interna_ecommerce.tpl');
            break;

        case 'interna_mvp':

            $smarty->assign("pagina",'servicos');
            $smarty->display('interna_mvp.tpl');
            break;

        case 'interna_marketplace':

            $smarty->assign("pagina",'solucoes');
            $smarty->display('interna_marketplace.tpl');
            break;

        case 'interna_projetos':

            $smarty->assign("pagina",'solucoes');
            $smarty->display('interna_projetos.tpl');
            break;

        case 'interna_chamados':

            $smarty->assign("pagina",'solucoes');
            $smarty->display('interna_chamados.tpl');
            break;

        case 'interna_mei':

            $smarty->assign("pagina",'solucoes');
            $smarty->display('interna_mei.tpl');
            break;

        case 'template_agency':
            $smarty->assign("pagina",'templates');
            $smarty->display('paginas_internas/interna_agency.tpl');
        break;

        case 'template_freela':
            $smarty->assign("pagina",'templates');
            $smarty->display('paginas_internas/interna_freelancer.tpl');
        break;

        case 'template_creative':
            $smarty->assign("pagina",'templates');
            $smarty->display('paginas_internas/interna_creative.tpl');
        break;

        case 'template_gray':
            $smarty->assign("pagina",'templates');
            $smarty->display('paginas_internas/interna_gray.tpl');
        break;

        case 'template_casual':
            $smarty->assign("pagina",'templates');
            $smarty->display('paginas_internas/interna_casual.tpl');
        break;

        case 'template_stylish':
            $smarty->assign("pagina",'templates');
            $smarty->display('paginas_internas/interna_stylish.tpl');
        break;

  case 'template_business':
            $smarty->assign("pagina",'templates');
            $smarty->display('paginas_internas/interna_business.tpl');
        break;

  case 'template_corlate':
            $smarty->assign("pagina",'templates');
            $smarty->display('paginas_internas/interna_corlate.tpl');
        break;

  case 'template_knight':
            $smarty->assign("pagina",'templates');
            $smarty->display('paginas_internas/interna_knight.tpl');
        break;

        default:
            $smarty->assign("pagina",'home');
            $smarty->display('home.tpl');

    }
}else{
    $smarty->assign("pagina",'home');
    $smarty->display('home.tpl');
}

